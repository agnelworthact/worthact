<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ($comment_type === 'child') : if (count($comments) > 0) : foreach ($comments as $comment) : ?>

<div class = "comments media media-list media-list-bordered" data-id="<?= $comment->comment_id ?>">
    <?php $this->hook->check_user_comment($comment->comment_id, ''); ?>
    <div class = "media-left">
        <a href="<?= base_url('dashboard/profile/' . $comment->comment_user_id) ?>"><img alt = "<?= ($comment->user_type == 1) ? ucfirst($comment->firstname) . ' ' . ucfirst($comment->lastname) : $comment->name; ?>" src="<?= base_url(($comment->propic != '') ? "assets/userdata/dashboard/propic/" . $comment->propic : "assets/img/user_placeholder.png") ?>" class="shadow"></a>
    </div>
    <div class = "media-body">
        <a href="<?= base_url('dashboard/profile/' . $comment->comment_user_id) ?>"><h4 class = "media-heading"><?= ($comment->user_type == 1) ? ucfirst($comment->firstname) . ' ' . ucfirst($comment->lastname) : $comment->name; ?></h4></a>
        <p><?= $comment->comment ?></p>
    </div>
</div>

<?php endforeach; endif; else :

if (count($comments) > 0) : foreach ($comments as $comment) : ?>

<div class = "comments media media-list media-list-bordered" data-id="<?= $comment->comment_id ?>">
    <?php $this->hook->check_user_comment($comment->comment_id, $type_id); ?>
    <div class = "media-left">
        <a href="<?= base_url('dashboard/profile/' . $comment->comment_user_id) ?>"><img alt = "<?= ($comment->user_type == 1) ? ucfirst($comment->firstname) . ' ' . ucfirst($comment->lastname) : $comment->name; ?>" src="<?= base_url(($comment->propic != '') ? "assets/userdata/dashboard/propic/" . $comment->propic : "assets/img/user_placeholder.png") ?>" class="shadow"></a>
    </div>
    <div class = "media-body">
        <a href="<?= base_url('dashboard/profile/' . $comment->comment_user_id) ?>"><h4 class = "media-heading"><?= ($comment->user_type == 1) ? ucfirst($comment->firstname) . ' ' . ucfirst($comment->lastname) : $comment->name; ?></h4></a>
        <p><?= $comment->comment ?></p>
        <div class = "media-annotation">
            <?php $this->hook->comment_replies($comment->comment_id); ?>
            <a onclick="comment_reply(<?= $comment->comment_id ?>)"><i class="ion-reply"></i> Reply</a>
        </div>
    </div>
    <div class="comment-child"></div>
    <div class="comment-reply"></div>
</div>

<?php endforeach; endif; endif; ?>