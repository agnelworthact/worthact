<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container offers seso">
    <div class="row">
        <img src="<?= base_url('assets/img/seso.jpg'); ?>" class="img-responsive offer-banner"  alt="offer" />
        <div class="col-sm-12 col">
            <div class="row type">
                <div class="col-sm-12">
                    <div class="offer_modal">
                        <div class="modal-content shadow">
                            <div class="modal-header">
                                <h4 class="modal-title text-center">Welcome to WorthAct SESO!</h4>
                                <h4 class="text-center title-two">Save the Earth Save Ourselves</h4>
                            </div>
                            <div class="modal-body top_zero">
                                <p class="entry">The beginning of a new academic year. Young's minds fresh and rejuvenated from the vacation, on the lookout for new experiences and in search of advanced courses to move up in life, to find the road to prosperity. To instil in them a touch of nature, a realisation that mankind depends wholly on nature for existence, we have to get them more involved in eco-friendly activities and awareness programmes.</p>
                                <p class="entry">WorthAct introduces a talent search competition <strong>"WorthAct SESO"</strong> aimed at School/College students where they can showcase their expertise, win prizes and at the same time learn to be more environment friendly and responsible.</p>
                                <h4 class="text-center how main">What you can do</h4>
                                <h5 class="text-center">Select any the following</h5>
                                <div class="row how_action text-center">
                                    <div class="box col-sm-4">
                                        <div class="shadow cursor">
                                            <img src="<?= base_url('assets/img/quil.png') ?>" alt="Plant Trees" />
                                            <h3>ESSAY COMPETITION</h3>
                                            <p>The Contestants shall choose a topic from above and write an essay that shall be in a minimum of 700 words and not exceeding 800.</p>
                                        </div>
                                    </div>
                                    <div class="box col-sm-4">
                                        <div class="shadow cursor">
                                            <img src="<?= base_url('assets/img/draw.png') ?>" alt="Refer members" />
                                            <h3>DRAWING COMPETITION</h3>
                                            <p>The Contestants are expected to produce original drawings/paintings depicting topics/situations relevant to our Environment.</p>
                                        </div>
                                    </div>
                                    <div class="box col-sm-4">
                                        <div class="shadow cursor">
                                            <img src="<?= base_url('assets/img/camera.png') ?>" alt="Refer members" />
                                            <h3>CREATIVE PHOTOGRAPHY</h3>
                                            <p>All Photographs should be environment related and should carry a strong message to the public about the current state of our Planet and its Environment.</p>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="text-center how">All the above competitions shall be centered around the following topics :-</h4>
                                <ul>
                                    <li>Eco-friendly lifestyle, to support our planet.</li>
                                    <li>Domestic eco- friendly rain water harvesting</li>
                                    <li>Waste management at home.</li>
                                </ul>
                                <div class="offer_action text-center">
                                    <h3>Join...Win...Spread the Word...Save the World</h3>
                                </div>
                                <div class="text-center">
                                    <a class="link" data-toggle="modal" data-target="#seso_terms">Terms & Conditions</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('dashboard/modals/seso_terms'); ?>


