<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div id="seso_terms" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Terms & Conditions</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li>This is an online competition and all entries shall be posted at www.worthact.com.</li>
                    <li>All Contestants shall register as a Free/Premium member at www.worthact.com to be able to participate.</li>
                    <li>The period of competition shall be from June 15 2017 to June 25 2017.</li>
                    <li>Entries must be posted on site before 11:59 PM on the 25th of June 2017.</li>
                    <li>When submitting your entry, you must provide your Name, Age, Class, School/College, and the name and contact number of your Parent/Guardian.</li>
                    <li>Only one entry per student per Competition shall be allowed.</li>
                    <li>All entries shall be original, imaginative, and free from plagiarism and 100% unique.</li>
                    <li>Winners shall be selected by an expert panel. The decision of the Panel will be final and no correspondence shall be entered into on the matter.</li>
                    <li>The Winners shall be announced in WorthAct on</li>
                    <li>The Winners from each Competition shall be awarded a cash price of INR 15000 each.</li>
                    <li>The Prize for the competition is not transferable and cannot be exchanged for any other item.</li>
                    <li>Sending and entry to the competition means you have the approval of School/College authorities and your Parents.</li>
                    <li>All Contestants and Winners shall agree to publish their entries and personal details on www.worthact.com.</li>
                    <li>Submitting an entry to the Competition means that you, your School Authorities and your Parents have read and agreed to these Terms and Conditions which are also the rules of the Competition.</li>
                </ul>
            </div>
        </div>
    </div>    
</div>