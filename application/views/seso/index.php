<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <title>:: SESO - WorthACT ::</title>
        <meta name="description" content="Double your chances and become a winner.">
        <meta name="keywords" content="offers,iphone,ps4,campaign,free,sos,worthact,win,prizes,promotion,limited,bumper,wow">
        <meta name="author" content="worthact">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="og:url"                content="http://worthact.com" />
        <meta property="og:type"               content="website" />
        <meta property="og:title"              content="For a better world" />
        <meta property="og:description"        content="A platform to network with noble individuals who can share and unveil the endless possibilities to make this world a better place again." />
        <meta property="og:image"              content="https://s27.postimg.org/mmb1xhamr/worthact.png" />
        <meta property="og:image:width"        content="450"/>
        <meta property="og:image:height"       content="298"/>
        <meta property="fb:app_id"             content="220651215058434" />
        <link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png'); ?>" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,700,700i,900" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url('assets/css/plugins.css'); ?>" type='text/css'>
        <link href="<?= base_url('assets/seso/style.css') ?>" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <nav class="top">
            <div class="container">
                <div class="pull-left">
                    <a href="<?= base_url() ?>" title="register"><img class="logo" src="<?= base_url('assets/img/logo-orange.png') ?>" alt="worthact" /></a>
                </div>
                <ul class="list-inline pull-right student">
                    <li><a href="<?= base_url() ?>" title="register">Register</a></li>
                    <li><a href="<?= base_url('login') ?>" title="login">Login</a></li>
                    <li><a href="<?= base_url('about') ?>" title="about">About</a></li>
                </ul>
            </div>
        </nav>
        <div class="container landing-container" id="banner">
            <div class="section-banner">
                <img src="<?= base_url('assets/seso/bulb.png') ?>" class="img-responsive img-lightbulb" />
                <div class="banner-content">
                    <img src="<?= base_url('assets/seso/banner.png') ?>" class="img-responsive img-banner" />
                    <div class="bg-white-xs">
                        <div class="text-description">
                            <p style="text-align: center; ">WorthAct introduces a talent search competition <strong>"WorthAct SESO"</strong> aimed at School/College students where they can showcase their expertise, win prizes and at the same time learn to be more environment friendly and responsible.</p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row-button">
                            <button class="btn btn-primary btn-lg btn-join-now" data-toggle="modal" data-target="#register_acc">Register Now</button>
                        </div>
                        <div class="col-left">
                            <div class="text-label">Submit your essay entries from</div>
                            <div class="text-duration">1 - 30 July 2017</div>
                            <div>*<a href="" data-toggle="modal" data-target="#seso_terms">Terms & Conditions</a> apply</div>
                        </div>
                        <div class="line-middle"></div>
                        <div class="col-right">
                            <img src="<?= base_url('assets/seso/win.png') ?>" class="img-responsive" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="section-category">
            <div class="container landing-container">
                <div class="category-left">
                    <div class="row-quote">
                        <img class="img-responsive img-square" src="<?= base_url('assets/seso/box.png') ?>" />
                        <div class="text-title">
                            The beginning of a new academic year. Young's minds fresh and rejuvenated from the vacation, on the lookout for new experiences and in search of advanced courses to move up in life, to find the road to prosperity.
                        </div>
                    </div>
                </div>
                <div class="category-right">
                    <div class="row-quote">
                        <img class="img-responsive img-square" src="<?= base_url('assets/seso/box.png') ?>" />
                        <div class="text-title">
                            To instil in them a touch of nature, a realisation that mankind depends wholly on nature for existence, we have to get them more involved in eco-friendly activities and awareness programmes.
                        </div>
                    </div>
                </div>
                <div class="wire"></div>
                <img src="<?= base_url('assets/seso/laptop.png') ?>" class="img-responsive img-laptop" />
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="bg-white">
            <div class="container landing-container" id="prizes">
                <div class="section-prizes">
                    <h3 class="text-center">WHAT YOU CAN DO</h3>
                    <div class="row">
                        <div class="col-sm-4 col text-center">
                            <div class="box">
                                <img src="<?= base_url('assets/img/quil.png') ?>" alt="essay" />
                                <h5>ESSAY COMPETITION</h5>
                                <p>The Contestants shall choose a topic from above and write an essay that shall be in a minimum of 700 words and not exceeding 800.</p>
                            </div>
                        </div>
                        <div class="col-sm-4 col text-center">
                            <div class="box">
                                <img src="<?= base_url('assets/img/draw.png') ?>" alt="drawing" />
                                <h5>DRAWING COMPETITION</h5>
                                <p>The Contestants are expected to produce original drawings/paintings depicting topics/situations relevant to our Environment.</p>
                            </div>
                        </div>
                        <div class="col-sm-4 col text-center">
                            <div class="box">
                                <img src="<?= base_url('assets/img/camera.png') ?>" alt="photo" />
                                <h5>CREATIVE PHOTOGRAPHY</h5>
                                <p>All Photographs should be environment related and should carry a strong message to the public about the current state of our Planet and its Environment.</p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <footer>
            <div class="container">
                <div>
                    <a href="https://www.facebook.com/worthactofficial/" style="margin:0 15px;" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://plus.google.com/+worthact" style="margin:0 15px;" target="_blank"><i class="fa fa-google-plus"></i></a>
                    <a href="https://twitter.com/worthact" style="margin:0 15px;" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.linkedin.com/company/worthact" style="margin:0 15px;" target="_blank"><i class="fa fa-linkedin"></i></a>
                    <a href="https://www.youtube.com/worthact" style="margin:0 15px;" target="_blank"><i class="fa fa-youtube"></i></a>
                    <a href="https://www.instagram.com/worthact/" style="margin:0 15px;" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="http://blog.worthact.com/" style="margin:0 15px;" target="_blank"><i class="fa fa-rss-square"></i></a>
                </div>
                <div>&copy; <?= date('Y') ?> <strong>WorthACT</strong> - 2017. All Rights Reserved</div>
            </div>
        </footer>
        <div id="seso_terms" class="modal fade" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Terms & Conditions</h4>
                    </div>
                    <div class="modal-body">
                        <ul>
                            <li>This is an online competition and all entries shall be posted at www.worthact.com.</li>
                            <li>All Contestants shall register as a Free/Premium member at www.worthact.com to be able to participate.</li>
                            <li>The period of competition shall be from June 15 2017 to June 25 2017.</li>
                            <li>Entries must be posted on site before 11:59 PM on the 25th of June 2017.</li>
                            <li>When submitting your entry, you must provide your Name, Age, Class, School/College, and the name and contact number of your Parent/Guardian.</li>
                            <li>Only one entry per student per Competition shall be allowed.</li>
                            <li>All entries shall be original, imaginative, and free from plagiarism and 100% unique.</li>
                            <li>Winners shall be selected by an expert panel. The decision of the Panel will be final and no correspondence shall be entered into on the matter.</li>
                            <li>The Winners shall be announced in WorthAct on</li>
                            <li>The Winners from each Competition shall be awarded a cash price of INR 15000 each.</li>
                            <li>The Prize for the competition is not transferable and cannot be exchanged for any other item.</li>
                            <li>Sending and entry to the competition means you have the approval of School/College authorities and your Parents.</li>
                            <li>All Contestants and Winners shall agree to publish their entries and personal details on www.worthact.com.</li>
                            <li>Submitting an entry to the Competition means that you, your School Authorities and your Parents have read and agreed to these Terms and Conditions which are also the rules of the Competition.</li>
                        </ul>
                    </div>
                </div>
            </div>    
        </div>
        <div class="modal fade" id="register_acc" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="offer-register">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            <h4 class="modal-title">Register Now</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Email</label>
                                <input id="register-email" class="trans form-control" type="email" name="email" placeholder="Email" required="required" onblur="validate_user_email(this.value)" />
                            </div>    
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="trans form-control" name="password" placeholder="Password" required="required" />
                            </div>
                            <div class="form-group">
                                <button data-ripple class="full-width shadow" type="submit">Get Started</button>
                                <img src="<?= base_url('assets/img/reload-2.svg') ?>" id="loader" alt="loader" />
                            </div>
                            <p class="text-center text-policy">By registering, you agree to WorthAct's <br /><a href="<?= base_url('privacy_policy'); ?>">Privacy Policy</a> and <a href="<?= base_url('terms_and_conditions'); ?>">Terms & Conditions</a></p>
                            <div class="alert alert-danger shadow" role="alert"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <script src="<?= base_url('assets/js/plugins.js'); ?>" type='text/javascript'></script>
        <script>
            function validate_user_email(email) {
                $('#offer-register .alert').hide().text('');
                $.post('<?= base_url(); ?>home/validateemail', {email: email}, function (data) {
                    if (data == 'invalid') {
                        document.getElementById('register-email').value = '';
                        $('#offer-register .alert').show().text('Email id already taken.');
                    }
                });
            }
            (function($) {
                $('#offer-register').submit(function (e) {
                    e.preventDefault();
                    $('#offer-register #loader').fadeIn();
                    $('#offer-register .alert').hide();
                    $('#offer-register button').prop('disabled', true);
                    var form = document.getElementById('offer-register');
                    var fd = new FormData(form);
                    $.ajax({
                        url: '<?= base_url(); ?>home/register_offer',
                        type: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data == 'success') {
                                window.location =  '<?= base_url(); ?>registration_success';
                            } else {
                                $('#offer-register .alert').html(data).show();
                            }
                            $('#offer-register #loader').fadeOut();
                            $('#offer-register button').prop('disabled', false);
                        }
                    });
                });
            }(jQuery));
        </script>
    </body>
</html>