<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Seso extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->session->set_userdata('enroll', 1);
        $this->load->view('seso/index');
    }
    
}